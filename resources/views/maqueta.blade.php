@extends('layouts.simple')

@section('content')

	<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 p-3 bg-info">
			<a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{asset('img/logo.png')}}" height="80">
            </a>
		</div>
		<div class="col-md-12 bg-dark">
			<nav class="navbar navbar-expand-lg text-white">
				 
				<button class="navbar-toggler bg-white" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="navbar-toggler-icon "></span>
				</button> 
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="navbar-nav">
						<li class="nav-item active ">
							 <a class="nav-link text-white" href="#">Inicio <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item ">
							 <a class="nav-link text-white" href="#">Tendencia</a>
						</li>
						<li class="nav-item dropdown ">
							<a class="nav-link dropdown-toggle text-white" href="#" 
							id="navbarDropdownMenuLink" data-toggle="dropdown">Más categorias</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
								 <a class="dropdown-item" href="#">Deportes</a> 
								 <a class="dropdown-item" href="#">Tecnología</a> 
								 <a href="#" class=" dropdown-item dropdown-toggle" data-toggle="dropdown">Nacional</a>
	                            <ul class="dropdown-menu">
	                                <li><a class="dropdown-item">Action</a></li>
	                                <li class="divider"></li>
	                                <li class="dropdown-submenu">
	                                    <a href="#" class="dropdown-item dropdown-toggle" data-toggle="dropdown">Dropdown</a>
	                                    <ul class="dropdown-menu">
	                                       <li><a>Action</a></li>
	                                    </ul>
	                                </li>
	                            </ul>
							</div>
						</li>
					</ul>
					<ul class="navbar-nav ml-md-auto d-none d-sm-block">
						<li>
							<form class="form-inline">
								<input class="form-control mr-sm-2" type="text"> 
								<button class="btn btn-success my-2 my-sm-0" type="submit">
									Buscar noticias
								</button>
							</form>
						</li>
					</ul>
				</div>
			</nav>
			<div class="d-block d-sm-none" >
				<form class="form-inline" align="center">
					<input class="form-control col-9 mr-sm-2 mr-2" type="text"> 
					<button class="btn btn-success my-2 my-sm-0" type="submit">
						Buscar
					</button>
				</form>
			</div>
		</div>
	</div>

	<div class="row m-3">
		<div class="col-md-12 ">
			<div class="row">
				<div class="col-md-8">
					<div class="jumbotron">
						<h2>
							Ballena de 12 metros hecha de plásticos es arte en Bélgica
						</h2>
						<p>
							En la Trienal de Brujas, en Bélgica, se instaló la figura de ballena de 12 metros de altura construida con cinco toneladas de desechos...
						</p>
						<p>
							<a class="btn btn-primary btn-large" href="#">Leer más</a>
						</p>
					</div>
					<div class="row">
						<div class="col-md-5 bg-white p-3 mt-2 ml-2 mr-1">
							<h2>
								Silleteros recorren las calles de Nueva York este domingo
							</h2>
							<p>
								En Queens, Nueva York, avanza la celebración del Flower Festival 2018, encuentro anual de la colonia colombiana en esa ciudad, que...
							</p>
							<p>
								<a class="btn" href="#">Ver detalles »</a>
							</p>
						</div>
						<div class="col-md-5 bg-white p-3 mt-2 ml-2 mr-1">
							<h2>
								Ojo: la silla de niños sí es importante en el carro
							</h2>
							<p>
								No es solo montarlos a pasear. Su seguridad debe ser una prioridad, adaptada a sus necesidades. No es comprar cualquier silla en descuento....
							</p>
							<p>
								<a class="btn" href="#">Ver detalles »</a>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5 bg-white p-3 mt-2 ml-2 mr-1">
							<h2>
								Así llegaron los primeros perros a Estados Unidos según un estudio
							</h2>
							<p>
								Los primeros perros que habitaron Estados Unidos llegaron de la vasta región de Siberia (Rusia) hace unos 9.000 años y desaparecieron...
							</p>
							<p>
								<a class="btn" href="#">Ver detalles »</a>
							</p>
						</div>
						<div class="col-md-5 bg-white p-3 mt-2 ml-2 mr-1">
							<h2>
								Hoy es el día en que la Tierra está más lejos del Sol
							</h2>
							<p>
								Hoy a las 11:47 de la mañana hora local, la Tierra estará en su punto más alejado del Sol de todo el año. A esa hora se encontrará...
							</p>
							<p>
								<a class="btn" href="#">Ver detalles »</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 mt-3">
					<h2>
						Última hora
					</h2>
					<div class="row">
						<div class="col-md-12">
							<h3>Deportes</h3>
						</div>
						<div  class="col-md-6 col-4 p-2">
							<img alt="Bootstrap Image Preview" src="https://www.layoutit.com/img/sports-q-c-140-140-3.jpg">
						</div>
						<div  class="col-md-6 col-4 p-2">
							<img alt="Bootstrap Image Preview" src="https://www.layoutit.com/img/sports-q-c-140-140-3.jpg">
						</div>
						<div  class="col-md-6 col-4 p-2">
							<img alt="Bootstrap Image Preview" src="https://www.layoutit.com/img/sports-q-c-140-140-3.jpg">
						</div>
						<div  class="col-md-6 col-4 p-2">
							<img alt="Bootstrap Image Preview" src="https://www.layoutit.com/img/sports-q-c-140-140-3.jpg">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h3>Entretenimiento</h3>
						</div>
						<div  class="col-md-6 col-4 p-2">
							<img alt="Bootstrap Image Preview" src="https://www.layoutit.com/img/sports-q-c-140-140-3.jpg">
						</div>
						<div  class="col-md-6 col-4 p-2">
							<img alt="Bootstrap Image Preview" src="https://www.layoutit.com/img/sports-q-c-140-140-3.jpg">
						</div>
						<div  class="col-md-6 col-4 p-2">
							<img alt="Bootstrap Image Preview" src="https://www.layoutit.com/img/sports-q-c-140-140-3.jpg">
						</div>
						<div  class="col-md-6 col-4 p-2">
							<img alt="Bootstrap Image Preview" src="https://www.layoutit.com/img/sports-q-c-140-140-3.jpg">
						</div>
					</div>
					<div class="page-header">
						<h1>
							Más categorías
						</h1>
					</div>
					<ul>
						<li class="list-item">
							Cultura
						</li>
						<li class="list-item">
							Política
						</li>
						<li class="list-item">
							Negocios
						</li>
						<li class="list-item">
							Internacional
						</li>
						<li class="list-item">
							Colombia
						</li>
					</ul>
				</div>
			</div>
			<div class="row m-3 d-none d-sm-block">
				<div class="col-md-12">
					<div class="carousel slide" id="carousel-336885">
						<ol class="carousel-indicators">
							<li data-slide-to="0" data-target="#carousel-336885">
							</li>
							<li data-slide-to="1" data-target="#carousel-336885" class="active">
							</li>
							<li data-slide-to="2" data-target="#carousel-336885">
							</li>
						</ol>
						<div class="carousel-inner">
							<div class="carousel-item active carousel-item-left">
								<img class="d-block w-100" alt="Carousel Bootstrap First" src="https://www.layoutit.com/img/sports-q-c-1600-500-1.jpg">
								<div class="carousel-caption">
									<h4>
										First Thumbnail label
									</h4>
									<p>
										Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
									</p>
								</div>
							</div>
							<div class="carousel-item carousel-item-next carousel-item-left">
								<img class="d-block w-100" alt="Carousel Bootstrap Second" src="https://www.layoutit.com/img/sports-q-c-1600-500-2.jpg">
								<div class="carousel-caption">
									<h4>
										Second Thumbnail label
									</h4>
									<p>
										Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
									</p>
								</div>
							</div>
							<div class="carousel-item">
								<img class="d-block w-100" alt="Carousel Bootstrap Third" src="https://www.layoutit.com/img/sports-q-c-1600-500-3.jpg">
								<div class="carousel-caption">
									<h4>
										Third Thumbnail label
									</h4>
									<p>
										Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
									</p>
								</div>
							</div>
						</div> <a class="carousel-control-prev" href="#carousel-336885" data-slide="prev"><span class="carousel-control-prev-icon"></span> <span class="sr-only">Previous</span></a> <a class="carousel-control-next" href="#carousel-336885" data-slide="next"><span class="carousel-control-next-icon"></span> <span class="sr-only">Next</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 p-3 bg-info">
			<h3 class="text-center text-white">LaraCamp 2018</h3>
		</div>
	</div>

</div>

@stop