@extends('layouts.app')

@section('content')

	<div class="container">

		<div class="row">

			<div class="col-md-12">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <div class="breadcrumb-item active" aria-current="page">
				    	<h1 class="d-inline">Campamentos </h1>
				    	&nbsp;
				    	<a  href="{{route('campamento.create')}}" class="btn btn-success btn-lg float-right">Crear</a>
				    </div>
				  </ol>
				</nav>
			</div>

			<div class="col-md-12">
				<table class="table" style="background: #fff">
					<thead class="thead-light">
						<tr>
							<th width="40%">Nombre</th>
							<th>Ciudad</th>
							<th>Hectáreas</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						@if($data->count())
							@foreach($data as $value)
								<tr>
							      <td>{{$value->nombre}}</td>
							      <td>{{$value->ciudad->nombre}}</td>
							      <td>{{$value->hectareas}}</td>
							      <td>
							      	<a href="{{route('campamento.edit',$value->id)}}" class="btn btn-warning">Editar</a>
							      	<a href="{{route('campamento.show',$value->id)}}" class="btn btn-info">Ver</a>
							      	<form class="d-inline" method="post" action="{{route('campamento.destroy',$value->id)}}">
							      		@method("DELETE")
							      		@csrf
							      		<button class="btn btn-danger">Eliminar</button>
							      	</form>
							      </td>
							    </tr>
							@endforeach
						@else
							<tr>
								<td colspan="3">
					    			Sin registros
								</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>	

		</div>
	</div>

@stop