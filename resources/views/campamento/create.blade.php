@extends('layouts.app')

@section('content')

	<div class="container">

		<div class="row">

			<div class="col-md-12">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    	<h1 >Crear campamento </h1>
				  </ol>
				</nav>
			</div>

			<div class="col-md-12">
				<div class="card p-2">
					
				<form method="post"  action="{{ $method }}">
					<div class="row">
						@csrf
						@if(isset($event))
							@method($event)
						@endif

						<div class="form-group col-md-6">
							<label for="exampleInputEmail1">Nombre</label>
							<input type="text" value="{{ old('nombre') ?: $campamento->nombre  }}" name="nombre" class="form-control" required >
						</div>

						<div class="form-group col-md-6">
							<label for="exampleInputPassword1">Correo</label>
							<input type="email" value="{{ old('correo') ?: $campamento->correo  }}" name="correo"  class="form-control" required>
						</div>

						<div class="form-group col-md-6">
							<label for="exampleInputEmail1">Departamento</label>
							<select required class="form-control" id="departamento">
								<option></option>
								@foreach($dep as $item)
									<option value="{{$item->id}}">{{$item->nombre}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-md-6">
							<label for="exampleInputPassword1">Ciudad</label>
							<select required disabled class="form-control" id="ciudad">
								@if(isset($campamento->ciudad->nombre))
									<option>{{$campamento->ciudad->nombre}}</option>
								@endif
							</select>
							<input type="hidden" value="{{ old('ciudad_id') ?: $campamento->ciudad_id  }}" name="ciudad_id" id="ciudad_id" >
						</div>

						<div class="form-group col-md-4">
							<label for="exampleInputPassword1">Hectáreas</label>
							<input type="number" value="{{ old('hectareas') ?: $campamento->hectareas  }}" name="hectareas" class="form-control" required>
						</div>

						<div class="form-group  ubicacion col-md-4" style="display: {{isset($campamento->ciudad_id) ? 'block' : 'none'}};">
							<label for="exampleInputPassword1">Latitud</label>
							<input type="text" value="{{ old('latitud') ?: $campamento->latitud  }}" name="latitud" class="form-control" required>
						</div>

						<div class="form-group  ubicacion col-md-4" style="display: {{isset($campamento->ciudad_id) ? 'block' : 'none'}};">
							<label for="exampleInputPassword1">Longitud</label>
							<input type="text" value="{{ old('longitud') ?: $campamento->longitud  }}" name="longitud" class="form-control" required>
						</div>

						<div class="col-md-12">
				            <br>
				            @if(isset($event) and $event!='SHOW')
								<button type="submit" class="btn btn-primary">Guardar</button>
							@endif
							<a href="{{route('campamento.index')}}" class="btn btn-info">Volver</a>
						</div>
					</div>
				</form>
				<br>
					@if ($errors->has('nombre'))
		                  <span class="alert bg-danger"><b>Nombre incorrecto, este valor solo permite texto.</b></span>
		            @endif
					@if ($errors->has('coreo'))
		                  <div class="alert bg-danger"><b>Correo invalido.</b></div>
		            @endif
					@if ($errors->has('latitud'))
		                  <div class="alert bg-danger"><b>Latitud no tiene el formato correcto.</b></div>
		            @endif
					@if ($errors->has('longitud'))
		                  <div class="alert bg-danger"><b>Longitud no tiene el formato correcto.</b></div>
		            @endif
		            @if ($errors->has('hectareas'))
		                  <div class="alert bg-danger"><b>Hectáreas solo permite números.</b></div>
		            @endif
				</div>
			</div>	

		</div>
	</div>

@stop

@section('scripts')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script type="text/javascript">
		window.dpto = 0;
		window.preload = true;
		$(document).ready(function() {

			onloadDpto();

			@if(isset($campamento->departamento))
				window.preload = false;
				$('#departamento').val({{$campamento->departamento}}).trigger('change');
			@endif

		});

		function onloadDpto() {
			$('#departamento').select2().on('change', function () {
				    if(this.value.length){
				    	window.dpto = this.value;
				    	$('#ciudad').attr('disabled',false);
				    	if (window.preload) {
				    		$('#ciudad_id').val('');
				    		instanceCiudad();
						}
						window.preload=true;
				    }else{
				    	window.dpto = 0;
				    	$('#ciudad').attr('disabled',true);
				    	$('#ciudad_id').val('');
				    }
			});
		}

		function instanceCiudad() {
			$('#ciudad').select2({
			  ajax: {
			    url: '{{route('getCity')}}/'+window.dpto,
			    dataType: 'json',
			    data: function (params) {
			      var query = {
			        search: params.term,
			      }
			      return query;
			    },
			    processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.nombre,
		                        id: item.id
		                    }
		                })
		            };
		        }
			  }
			}).on('change', function () { 
				$('#ciudad_id').val(this.value);
				$('.ubicacion').show();
			});
			$("#ciudad").val(null).trigger('change');
			$('.ubicacion').hide();
		}

	</script>
@stop