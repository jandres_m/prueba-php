<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class GenerarCiudades extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$json = \Storage::disk('local')->get('colombia.min.json');
		$json = json_decode($json, true);

		foreach ($json as $key => $value) {
			$value = (object)$value;
			$id_dep = $this->setDepartamento($value->departamento);
			foreach ($value->ciudades as $ciudad) {
				$this->setCiudad($ciudad,$id_dep);
			}
		}		        
    }

    public function setDepartamento($name)
    {
    	$res = DB::table('departamentos')->insertGetId([
    		'nombre' => $name
    	]);
    	return $res;
    }
    public function setCiudad($name,$departamento)
    {
    	$res = DB::table('ciudades')->insertGetId([
    		'nombre' => $name,
    		'departamento_id' => $departamento
    	]);
    }
}
