<?php

namespace Campamento;

use Illuminate\Database\Eloquent\Model;

class Campamento extends Model
{
    protected $fillable = ['nombre','correo','ciudad_id','hectareas','latitud','longitud'];

    public function ciudad()
    {
    	return $this->belongsTo('Campamento\Ciudad','ciudad_id');
    }

}
