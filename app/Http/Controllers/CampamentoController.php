<?php

namespace Campamento\Http\Controllers;
use Campamento\Campamento;
use Campamento\Ciudad;

use Illuminate\Http\Request;
use Validator;

class CampamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Campamento::with('ciudad')->get();
        $title = 'Campamentos';
        $share = compact('data','title');
        return view('campamento.index',$share);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Crear campamento';
        $campamento=new Campamento();
        $method=route('campamento.store');
        $dep = \DB::table('departamentos')->get();
        $share = compact('title','dep','campamento','method');
        return view('campamento.create',$share);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'nombre' => 'alpha',
            'correo' => 'email',
            'hectareas' => 'numeric',
            'latitud' => 'numeric',
            'longitud' => 'numeric',
        ])->validate();

        Campamento::create($request->all());
        return redirect(route('campamento.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Ver campamento';
        $campamento= Campamento::find($id);
        $method='';
        $event = 'SHOW';
        $dep = \DB::table('departamentos')->get();
        $campamento->departamento = $campamento->ciudad->departamento_id;
        $share = compact('title','dep','campamento','method','event');
        return view('campamento.create',$share);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Editar campamento';
        $campamento= Campamento::find($id);
        $method=route('campamento.update',$id);
        $event = 'PATCH';
        $dep = \DB::table('departamentos')->get();
        $campamento->departamento = $campamento->ciudad->departamento_id;
        $share = compact('title','dep','campamento','method','event');
        return view('campamento.create',$share);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'nombre' => 'alpha',
            'correo' => 'email',
            'hectareas' => 'numeric',
            'latitud' => 'numeric',
            'longitud' => 'numeric',
        ])->validate();

        $campamento = Campamento::find($id);
        $campamento->update($request->all());

        return redirect(route('campamento.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Campamento::findOrFail($id)->delete();
        return back();
    }

    public function getCiudades($dpto,Request $request)
    {
        if($dpto>0){
            $res = Ciudad::where('departamento_id',$dpto)
                    ->where(function ($_this) use($request){
                        if($request->get('search')){
                            $_this->where('nombre','like',"%".$request->get('search')."%");
                        }
                    })
                    ->get(['id','nombre']);
            return response()->json($res,200);
        }else{
            return response()->json([],200);
        }
    }
}
